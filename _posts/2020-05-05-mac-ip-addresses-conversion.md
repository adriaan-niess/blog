---
layout: post
title:  "Python: How to map MAC and IPv4 addresses to/from integer values"
date:   2020-05-06 23:00:00 +0200
categories: python
permalink: /mac-ip-address-integer-conversion/
---

Representing MAC and IP addresses as numeric values is useful for calculations. Unfortunately in this form they're quite hard to read for humans, therefore the need for conversions. This is how it could be done:

{% highlight python %}
# address.py

def __bytes(value, num_bytes):
    for i in reversed(range(num_bytes)):
        yield value >> (8 * i) & 255

def __addr_to_int(addr, base, delimiter):
    segments = list(reversed(addr.split(delimiter)))
    result = 0
    for i in range(len(segments)):
        result += int(segments[i], base=base) * 256 ** i
    return result

def __addr_from_int(value, num_bytes, delimiter, formatter=lambda x: str(x)):
    return delimiter.join(map(formatter, __bytes(value, num_bytes)))

def mac_to_int(mac_addr):
    return __addr_to_int(mac_addr, base=16, delimiter=':')

def mac_from_int(value):
    return __addr_from_int(value, delimiter=':', num_bytes=6,
            formatter=lambda x: '{:02x}'.format(x))

def ipv4_to_int(ipv4_addr):
    return __addr_to_int(ipv4_addr, base=10, delimiter='.')

def ipv4_from_int(value):
    return __addr_from_int(value, delimiter='.', num_bytes=4)
{% endhighlight %}

Finally let's test the code

{% highlight ruby %}
# test.py

import unittest
from address import *

class Test(unittest.TestCase):
    def test_mac_addr_conversion(self):
        self.assertTrue(mac_from_int(mac_to_int('01:1b:19:00:00:00')) == '01:1b:19:00:00:00')

    def test_ipv4_addr_conversion(self):
        self.assertTrue(ipv4_from_int(ipv4_to_int('127.0.0.1')) == '127.0.0.1')

if __name__ == '__main__':
    unittest.main()
{% endhighlight %}
