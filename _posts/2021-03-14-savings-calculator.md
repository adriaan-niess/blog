---
layout: post
title:  "Savings calculator"
date:   2021-03-14 00:00:00 +0200
categories: 
permalink: /savings-calculator/
published: true
---

I created a small web application using React.js to calculate interest rates for various investments and evaluate different ETF/fund investment plans and more.

<iframe src="https://adriaan-niess.gitlab.io/savings-calculator/" title="Savings Calculator" width="100%" height="850px" style="border-width: 0; box-shadow: 0px 0px 1px 0px #000000;margin-bottom: 15px;"></iframe>

**How do I consider inflation?** - According to the fisher equation the nominal interest rate can be approximated by subtracting the inflation rate from the (real) interest rate. Therefore I didn't add an extra input field to the calculator.

**What about transaction fees?** - Just use net deposit rates. Unfortunately there are so many different pricing models that brokers are using and I didn't want to make the calculator too complicated by generalising them all.

**Financial transaction tax** must also be considered (at least in Germany). I didn't feel the need to include it in the calculator because the tax can simply be subtracted at the end.

**Where can I find the source code?** - You can find it [here](https://gitlab.com/adriaan-niess/savings-calculator) on Gitlab. It's available under the MIT license, so feel free to copy, modify or distribute it.
