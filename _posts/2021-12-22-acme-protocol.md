---
layout: post
title: Automatic Certificate Management Environment (ACME)
date: 2021-10-24 00:00:00 +0200
categories: 
permalink: /acme-protocol/
published: false
---

What is the Automatic Certificate Management Environment (ACME) protocol and how does it work? How can I automate issuance of certificates for my projects using Let's Encrypt? In this article I want to give a nitty-gritty explanation to these questions and cover the details required to implement your own [Certbot](https://certbot.eff.org/pages/about).

## Introduction

As an open standard (RFC8555) produced by the Internet Engineering Task Force (IETF), ACME makes it a lot easier for us to automate the issuance of digital X.509 certificates for Transport Layer Security (TLS) encryption. The most prominent certification authority (CA) Let's Encrypt provides these certificates free of charge via ACME. So ACME can not only be used to automate the issuance of certificates, but it's also comes with a lesser price tag. 

From a technical perspective, ACME allows *clients* to request certificate management actions, such as issuance and revocation, from a *server*. In this context a client is someone who controls a domain, usually a webserver or an entity that controls a webserver. The server on the other hand is a CA. In the first step the client authenticates an asymmetric key pair and proves ownership of the domain(s) to the server. Afterwards the private key is used by the client to sign requests sent to the server, while the server uses the public to authenticate them.

## Technical background

### Character encoding and message transport

The messages exchanged by ACME client and server are encoded into JSON objects carried over HTTPS. All messages must be encoded with UTF-8 character set. Binary fields use base64url encoding and don't allow trailing `=` characters.

### JSON Web Signature (JWS)

The ACME protocol uses JWS to sign and encode payloads. A typical JWS consists of header, payload and signature. The header and payload are JSON objects. For now we ignore the details (which header fields are valid) and give an example. We use the JWS header

```
jws_header = { "alg": "ES256" };
```

and the JWS payload

```
jws_payload = { "foo": "bar" };
```

We base64url encode both the header and the payload and omit the trailing `=` characters. Then we concat both strings with the `.` character. We end up with:

TODO

```
secret = <private-key>;
jws = base64_encode(jws_header) + '.' + base64_encode(jws_payload);
signature = base64_encode(sign(jws, secret));
jws = jws + '.' + signature;
```

TODO in most cases you would use a library for this
