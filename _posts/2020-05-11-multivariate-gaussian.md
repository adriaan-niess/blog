---
layout: post
title:  "Gaussian naive Bayes classifier"
date:   2020-05-11 01:33:00 +0200
categories: python
permalink: /gaussian-naive-bayes-classifier/
published: false
---

## Background

### Multivariate normal distribution

If a k-dimensional random vector $$X = (X_1, \dots, X_k)^T$$ is distributed according to a multivariate normal distribution, we can write

$$ X \sim N_k(\mu, \Sigma) $$

For the parameters of the distribution we have the mean vector

$$ \mu = E[X] = (E[X_1], \dots, E[X_k]) $$

and as counterpart to the standard deviation parameter that we would have in the one dimensional (univariate) case, we have the $$k \times k$$ covariance matrix

$$ \Sigma_{i,j} = E[(X_i - \mu_i)(X_j - \mu_j)] = \operatorname{Cov}[X_i, X_j] $$

The probability density function of a multivariate normal distributed is

$$ f(x) = \frac{1}{\sqrt{(2\pi)^k |\Sigma|}} \exp{\left( -\frac{1}{2}(x - \mu)^T \Sigma^{-1} (x - \mu) \right)}$$

and the maximum-likelihood estimate for the mean and covariance matrix are

$$ \hat{\mu} = \frac{1}{n}\ \sum_{i=1}^n x_i, \quad \hat{\Sigma} = \frac{1}{n} \sum_{i=1}^n (x_i - \hat{\mu}) (x_i - \hat{\mu})^T $$

### Naive Bayes classifier

A classifier is a function that maps objects $$o \in X$$ to classes $$c \in C$$

$$ \gamma: X \rightarrow C $$

A statistical classifier can be defined by selecting the class label with the highest probability given an object

$$ \gamma(o) = \underset{c \in C}{\operatorname{arg max}} P(c|o)$$

With the Bayes theorem the conditional probability $$P(c \vert o)$$ can be easily calculated from the known data

$$ P(c|o) = \frac{P(o|c) \cdot P(c)}{P(o)} = \frac{P(x_1, \dots, x_n|c) \cdot P(c)}{P(x_1, \dots, x_n)} $$

We also make the assumption that the values $$x_1, \dots, x_n$$ are stochastically independent from each other. Therefore the term *naive* Bayes. This will greatly simplify our calculations while only having minor impacts on the result. 

$$ = \frac{P(c)}{P(x_1, \dots, x_n)} \prod_{i=1}^n P(x_i | c) \propto P(c) \prod_{i=1}^n P(x_i | c) $$

## Example

The first thing when designing a classifier is to train it with training data. For this case, this means fitting a multivariate Gaussian distribution to each class label of the dataset. As an example I took the [iris flower dataset][iris-flower-dataset] and visualized the samples and their corresponding class with respect to only two features (petal lengh, petal width) in a scatter plot. There are 3 classes in total, that can be differentiated by their color. Then I plotted contour lines of the graph of the 3-dimensional Gaussian distributions for each class.

![alt text][gaussian-example]

If we take all four possible class attributes into account (sepal length/width, petal length/width), we'll end up with probability density functions (PDF) that expect a four dimensional input vector. With these PDFs we can create a naive Bayes classifier and evaluate it. For this sake the dataset must be split and I used $$\frac{3}{4}$$ of the data for training and $$\frac{1}{4}$$ for evaluating the model. Next we can create a confusion matrix for the classifier.

## Conclusion

dfdf


[iris-flower-dataset]: https://en.wikipedia.org/wiki/Iris_flower_data_set
[gaussian-example]: /img/multivariate-gaussian-example.png
